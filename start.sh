#!/bin/sh
set -e

cp .env.example .env

docker-compose down -v
docker-compose up -d

# waiting for all of services completely be ready
sleep 10;

docker-compose exec app composer install
docker-compose exec app php artisan migrate:fresh
docker-compose exec app php /var/www/vendor/phpunit/phpunit/phpunit --configuration /var/www/phpunit.xml --teamcity
docker-compose exec app php artisan migrate:fresh
