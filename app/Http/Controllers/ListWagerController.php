<?php

namespace App\Http\Controllers;

use App\Http\Resources\WagerResource;
use App\Services\ListWagerService;
use App\Services\Requests\ListWagerRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class ListWagerController
 * @package App\Http\Controllers
 */
class ListWagerController extends Controller
{
    /**
     * ListWagerController constructor.
     * @param ListWagerService $listWagerService
     */
    public function __construct(private ListWagerService $listWagerService)
    {
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function __invoke(Request $request): JsonResponse
    {
        $this->validateRequest($request);

        $wagers = $this->listWagerService->execute(new ListWagerRequest(
            (int) $request->get('page', 0),
            (int) $request->get('limit', 10),
        ));

        return $this->respondOK(WagerResource::collection($wagers));
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function validateRequest(Request $request)
    {
        $this->validate($request, [
            'page' => [
                'nullable',
                'integer',
                'gte:0',
            ],
            'limit' => [
                'nullable',
                'integer',
                'gt:0',
            ],
        ]);
    }
}
