<?php

namespace App\Http\Controllers;

use App\Exceptions\CreateWagerError;
use App\Exceptions\SellingPriceInvalid;
use App\Http\Resources\WagerResource;
use App\Services\CreateWagerService;
use App\Services\Requests\CreateWagerRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/**
 * Class CreateWagerController
 * @package App\Http\Controllers
 */
class CreateWagerController extends Controller
{
    /**
     * CreateWagerController constructor.
     * @param CreateWagerService $createWagerService
     */
    public function __construct(private CreateWagerService $createWagerService)
    {
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     * @throws SellingPriceInvalid
     * @throws CreateWagerError
     */
    public function __invoke(Request $request): JsonResponse
    {
        $this->validateRequest($request);

        $wager = $this->createWagerService->execute(new CreateWagerRequest(
            (int) $request->get('total_wager_value'),
            (int) $request->get('odds'),
            (int) $request->get('selling_percentage'),
            (float) $request->get('selling_price'),
        ));

        return $this->respondCreated(new WagerResource($wager));
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function validateRequest(Request $request): void
    {
        $this->validate($request, [
            'total_wager_value' => [
                'required',
                'integer',
                'gt:0',
            ],
            'odds' => [
                'required',
                'integer',
                'gt:0',
            ],
            'selling_percentage' => [
                'required',
                'integer',
                'between:1,100',
            ],
            'selling_price' => [
                'required',
                'numeric',
                'gt:0',
            ],
        ]);
    }
}
