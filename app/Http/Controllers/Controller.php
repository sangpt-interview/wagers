<?php

namespace App\Http\Controllers;

use App\Traits\Respondable;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    use Respondable;
}
