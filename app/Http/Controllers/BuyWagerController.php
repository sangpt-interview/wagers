<?php

namespace App\Http\Controllers;

use App\Http\Resources\BuyResource;
use App\Services\BuyWagerService;
use App\Services\Requests\BuyWagerRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Throwable;

/**
 * Class BuyWagerController
 * @package App\Http\Controllers
 */
class BuyWagerController extends Controller
{
    /**
     * BuyWagerController constructor.
     * @param BuyWagerService $buyWagerService
     */
    public function __construct(private BuyWagerService $buyWagerService)
    {
    }

    /**
     * @param Request $request
     * @param $wagerId
     * @return JsonResponse
     * @throws Throwable
     */
    public function __invoke(Request $request, $wagerId): JsonResponse
    {
        $request->merge(['wager_id' => $wagerId]);

        $this->validateRequest($request);

        $buy = $this->buyWagerService->execute(new BuyWagerRequest(
            (int) $wagerId,
            (float) $request->get('buying_price'),
        ));

        return $this->respondCreated(new BuyResource($buy));
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function validateRequest(Request $request)
    {
        $this->validate($request, [
            'wager_id' => [
                'required',
                Rule::exists('wagers','id')
            ],
            'buying_price' => [
                'required',
                'numeric',
                'gt: 0'
            ],
        ]);
    }
}
