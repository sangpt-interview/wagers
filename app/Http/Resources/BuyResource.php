<?php

namespace App\Http\Resources;

use App\Models\Buy;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class BuyResource
 * @package App\Http\Resources
 *
 * @mixin Buy
 */
class BuyResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'wager_id' => $this->wager_id,
            'buying_price' => $this->buying_price,
            'bought_at' => $this->created_at->timestamp,
        ];
    }
}
