<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Trait Respondable
 * @package App\Traits
 */
trait Respondable
{
    /**
     * @param $data
     * @return JsonResponse
     */
    public function respondCreated($data): JsonResponse
    {
        return response()->json($data, Response::HTTP_CREATED);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public function respondOK($data): JsonResponse
    {
        return response()->json($data, Response::HTTP_OK);
    }

    /**
     * @param string $message
     * @param int $httpCode
     * @return JsonResponse
     */
    public function respondError(string $message, int $httpCode): JsonResponse
    {
        return response()->json(['error' => $message], $httpCode);
    }
}