<?php

namespace App\Models\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

/**
 * Class FloatInTwoPlaces
 * @package App\Models\Casts
 */
class FloatInTwoPlaces implements CastsAttributes
{
    /**
     * @inheritDoc
     */
    public function get($model, $key, $value, $attributes)
    {
        return round($value, 2);
    }

    /**
     * @inheritDoc
     */
    public function set($model, $key, $value, $attributes)
    {
        return [$key => round($value, 2)];
    }
}