<?php

namespace App\Models\Casts;

/**
 * Class NullableFloatInTwoPlaces
 * @package App\Models\Casts
 */
class NullableFloatInTwoPlaces extends FloatInTwoPlaces
{
    /**
     * @inheritDoc
     */
    public function get($model, $key, $value, $attributes)
    {
        if (is_null($value)) {
            return null;
        }

        return parent::get($model, $key, $value, $attributes);
    }
}