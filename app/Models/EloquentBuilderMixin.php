<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;

/**
 * @method static static create(array $attributes = [])
 * @method static Builder|static where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static static find($id, $columns = ['*'])
 */
trait EloquentBuilderMixin
{
}