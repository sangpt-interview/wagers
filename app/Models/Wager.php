<?php

namespace App\Models;

use App\Models\Casts\FloatInTwoPlaces;
use App\Models\Casts\NullableFloatInTwoPlaces;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Wager
 * @package App\Models
 *
 * @property int $id
 * @property int $total_wager_value
 * @property int $odds
 * @property int $selling_percentage
 * @property float $selling_price
 * @property float $current_selling_price
 * @property float|null $percentage_sold
 * @property float|null $amount_sold
 * @property Carbon $created_at
 *
 * @mixin EloquentBuilderMixin
 */
class Wager extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'total_wager_value',
        'odds',
        'selling_percentage',
        'selling_price',
        'current_selling_price',
        'percentage_sold',
        'amount_sold',
    ];

    protected $casts = [
        'total_wager_value' => 'integer',
        'odds' => 'integer',
        'selling_percentage' => 'integer',
        'selling_price' => FloatInTwoPlaces::class,
        'current_selling_price' => FloatInTwoPlaces::class,
        'percentage_sold' => NullableFloatInTwoPlaces::class,
        'amount_sold' => NullableFloatInTwoPlaces::class,
    ];
}
