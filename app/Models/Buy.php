<?php

namespace App\Models;

use App\Models\Casts\FloatInTwoPlaces;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Buy
 * @package App\Models
 *
 * @property int $id
 * @property int $wager_id
 * @property float $buying_price
 * @property Carbon $created_at
 *
 * @mixin EloquentBuilderMixin
 */
class Buy extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'wager_id',
        'buying_price',
    ];

    protected $casts = [
        'wager_id' => 'integer',
        'buying_price' => FloatInTwoPlaces::class,
    ];
}
