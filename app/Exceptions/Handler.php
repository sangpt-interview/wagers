<?php

namespace App\Exceptions;

use App\Traits\Respondable;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use Respondable;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        BadRequestException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Throwable $exception
     * @return void
     *
     * @throws Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  Request  $request
     * @param Throwable $exception
     * @return Response|JsonResponse
     *
     * @throws Throwable
     */
    public function render($request, Throwable $e)
    {
        switch (get_class($e)) {
            case ValidationException::class:
                return $this->respondError(
                    json_encode($e->errors()),
                    Response::HTTP_UNPROCESSABLE_ENTITY
                );

            default :
                if (env('APP_DEBUG') === true) {
                    return parent::render($request, $e);
                }

                if ($this->shouldLogError($e)) {
                    Log::error($e->getMessage());
                    Log::error($e->getTraceAsString());
                }

                return $this->respondError(
                    $this->shouldShowErrorMessage($e) ? $e->getMessage() : 'Error.',
                    $e->getCode() ? $e->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR
                );
        }
    }

    protected function shouldLogError(Throwable $e): bool
    {
        return !in_array(get_class($e), [
            SellingPriceInvalid::class,
            BuyingPriceInvalid::class,
        ]);
    }

    protected function shouldShowErrorMessage(Throwable $e): bool
    {
        return in_array(get_class($e), [
            SellingPriceInvalid::class,
            BuyingPriceInvalid::class,
        ]);
    }
}
