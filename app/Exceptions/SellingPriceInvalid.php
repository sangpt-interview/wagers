<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

/**
 * Class SellingPriceInvalid
 * @package App\Exceptions
 */
class SellingPriceInvalid extends Exception
{
    /**
     * SellingPriceInvalid constructor.
     * @param float $min
     */
    public function __construct(float $min)
    {
        parent::__construct(
            "Selling price must be greater than {$min}",
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }
}