<?php

namespace App\Exceptions;

use Exception;

/**
 * Class CreateWagerError
 * @package App\Exceptions
 */
class CreateWagerError extends Exception
{
    protected $code = 500;
}