<?php

namespace App\Exceptions;

use Exception;

/**
 * Class BuyWagerError
 * @package App\Exceptions
 */
class BuyWagerError extends Exception
{
    protected $code = 500;
}