<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

/**
 * Class BuyingPriceInvalid
 * @package App\Exceptions
 */
class BuyingPriceInvalid extends Exception
{
    /**
     * BuyingPriceInvalid constructor.
     * @param float $currentSellingPrice
     */
    public function __construct(float $currentSellingPrice)
    {
        parent::__construct(
            "Buying price must be lesser or equal to {$currentSellingPrice}",
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }
}