<?php

namespace App\Services\Requests;

/**
 * Class BuyWagerRequest
 * @package App\Services\Requests
 */
class BuyWagerRequest
{
    /**
     * BuyWagerRequest constructor.
     * @param int $wagerId
     * @param float $buyingPrice
     */
    public function __construct(
        private int $wagerId,
        private float $buyingPrice,
    ) {
    }

    /**
     * @return int
     */
    public function getWagerId(): int
    {
        return $this->wagerId;
    }

    /**
     * @return float
     */
    public function getBuyingPrice(): float
    {
        return $this->buyingPrice;
    }
}
