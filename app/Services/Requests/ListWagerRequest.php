<?php

namespace App\Services\Requests;

/**
 * Class ListWagerRequest
 * @package App\Services\Requests
 */
class ListWagerRequest
{
    /**
     * ListWagerRequest constructor.
     * @param int $page
     * @param int $limit
     */
    public function __construct(
        private int $page,
        private int $limit,
    ) {
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }
}
