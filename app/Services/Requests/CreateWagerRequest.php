<?php

namespace App\Services\Requests;

/**
 * Class CreateWagerRequest
 * @package App\Services\Requests
 */
class CreateWagerRequest
{
    /**
     * CreateWagerRequest constructor.
     * @param int $totalWagerValue
     * @param int $odds
     * @param int $sellingPercentage
     * @param float $sellingPrice
     */
    public function __construct(
        private int $totalWagerValue,
        private int $odds,
        private int $sellingPercentage,
        private float $sellingPrice,
    ) {
    }

    /**
     * @return int
     */
    public function getTotalWagerValue(): int
    {
        return $this->totalWagerValue;
    }

    /**
     * @return int
     */
    public function getOdds(): int
    {
        return $this->odds;
    }

    /**
     * @return int
     */
    public function getSellingPercentage(): int
    {
        return $this->sellingPercentage;
    }

    /**
     * @return float
     */
    public function getSellingPrice(): float
    {
        return $this->sellingPrice;
    }
}
