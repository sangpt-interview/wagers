<?php

namespace App\Services;

use App\Models\Wager;
use App\Services\Requests\ListWagerRequest;
use Illuminate\Support\Collection;

/**
 * Class ListWagerService
 * @package App\Services
 */
class ListWagerService
{
    /**
     * @param ListWagerRequest $request
     * @return Collection
     */
    public function execute(ListWagerRequest $request): Collection
    {
        return Wager::query()
            ->limit($request->getLimit())
            ->offset($request->getPage() * $request->getLimit())
            ->get();
    }
}
