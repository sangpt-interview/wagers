<?php

namespace App\Services;

use App\Exceptions\CreateWagerError;
use App\Exceptions\SellingPriceInvalid;
use App\Models\Wager;
use App\Services\Requests\CreateWagerRequest;
use Throwable;

/**
 * Class CreateWagerService
 * @package App\Services
 */
class CreateWagerService
{
    /**
     * @param CreateWagerRequest $request
     * @return Wager
     * @throws SellingPriceInvalid
     * @throws CreateWagerError
     */
    public function execute(CreateWagerRequest $request): Wager
    {
        $this->validateSellingPrice($request);

        try {
            return Wager::create([
                'total_wager_value' => $request->getTotalWagerValue(),
                'odds' => $request->getOdds(),
                'selling_percentage' => $request->getSellingPercentage(),
                'selling_price' => $request->getSellingPrice(),
                'current_selling_price' => $request->getSellingPrice(),
            ]);
        } catch (Throwable $e) {
            throw new CreateWagerError($e->getMessage());
        }
    }

    /**
     * @param CreateWagerRequest $request
     * @throws SellingPriceInvalid
     */
    protected function validateSellingPrice(CreateWagerRequest $request)
    {
        $sellingPriceMin = $request->getTotalWagerValue() * $request->getSellingPercentage() / 100;

        if ($request->getSellingPrice() <= $sellingPriceMin) {
            throw new SellingPriceInvalid($sellingPriceMin);
        }
    }
}
