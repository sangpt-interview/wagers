<?php

namespace App\Services\Locks;

use Illuminate\Contracts\Cache\Lock;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Support\Facades\Cache;

/**
 * Class BuyWagerLock
 * @package App\Services\Locks
 */
class BuyWagerLock
{
    public const LOCK_EXPIRED_IN = 10; // If can't release the lock per expectation, the lock will automatically releases in 10 seconds.
    public const TIME_ACQUIRE_LOCK = 5; // If can not acquire the lock in 5 seconds, will timeout.

    /**
     * @var Lock
     */
    protected Lock $lock;

    /**
     * BuyWagerLock constructor.
     * @param int $wagerId
     */
    public function __construct(int $wagerId)
    {
        $this->lock = Cache::lock("BUY_WAGER_{$wagerId}_LOCK", config('lock.buy_wager.expired_id', 10));
    }

    /**
     * @return mixed
     *
     * @throws LockTimeoutException
     */
    public function block()
    {
        return $this->lock->block(config('lock.buy_wager.block_id', 5));
    }

    public function release()
    {
        $this->lock->release();
    }
}