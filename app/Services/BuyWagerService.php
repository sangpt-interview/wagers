<?php

namespace App\Services;

use App\Exceptions\BuyingPriceInvalid;
use App\Exceptions\BuyWagerError;
use App\Models\Buy;
use App\Models\Wager;
use App\Services\Locks\BuyWagerLock;
use App\Services\Requests\BuyWagerRequest;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Throwable;

/**
 * Class BuyWagerService
 * @package App\Services
 */
class BuyWagerService
{
    /**
     * @var BuyWagerLock
     */
    protected BuyWagerLock $lock;

    /**
     * @param BuyWagerRequest $request
     * @return Buy
     * @throws BuyingPriceInvalid
     * @throws Throwable
     */
    public function execute(BuyWagerRequest $request): Buy
    {
        $this->lock = app(BuyWagerLock::class, ['wagerId' => $request->getWagerId()]);

        try {
            $this->updateWager($request);

            $this->lock->release();

            return Buy::create([
                'wager_id' => $request->getWagerId(),
                'buying_price' => $request->getBuyingPrice(),
            ]);
        } catch (BuyingPriceInvalid|LockTimeoutException $e) {
            $this->lock->release();

            throw $e;
        } catch (Throwable $e) {
            $this->lock->release();

            throw new BuyWagerError($e->getMessage());
        }
    }

    /**
     * @param BuyWagerRequest $request
     * @throws BuyingPriceInvalid
     * @throws LockTimeoutException
     */
    protected function updateWager(BuyWagerRequest $request): void
    {
        $wager = Wager::find($request->getWagerId());

        if ($request->getBuyingPrice() > $wager->current_selling_price) {
            throw new BuyingPriceInvalid($wager->current_selling_price);
        }

        $this->lock->block();

        $wager->refresh();

        if ($request->getBuyingPrice() > $wager->current_selling_price) {
            throw new BuyingPriceInvalid($wager->current_selling_price);
        }

        $wager->current_selling_price -= $request->getBuyingPrice();
        $wager->amount_sold += $request->getBuyingPrice();
        $wager->percentage_sold = ($wager->amount_sold / $wager->selling_price) * 100;
        $wager->save();
    }
}