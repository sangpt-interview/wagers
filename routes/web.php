<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Http\Controllers\BuyWagerController;
use App\Http\Controllers\CreateWagerController;
use App\Http\Controllers\ListWagerController;
use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/wagers', CreateWagerController::class);
$router->post('/buy/{wagerId}', BuyWagerController::class);
$router->get('/wagers', ListWagerController::class);