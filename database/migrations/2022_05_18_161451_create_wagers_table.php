<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wagers', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('total_wager_value');
            $table->unsignedInteger('odds');
            $table->unsignedInteger('selling_percentage');
            $table->unsignedDecimal('selling_price');
            $table->unsignedDecimal('current_selling_price');
            $table->unsignedDecimal('percentage_sold')->nullable();
            $table->unsignedDecimal('amount_sold')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wagers');
    }
};
