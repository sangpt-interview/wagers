# `start.sh` one go command
- We cd to the root of project, and then run the command:
  ```
  ./start.sh
  ```
- It's working as CI
- We should see the unit test result as below:
  ```
  OK (47 tests, 151 assertions)
  ```
  
# List wager endpoint
- page
    - Must start at 0
    - The default value is 0 if it doesn't present.
    
- limit:
    - The default value is 10 if it doesn't present.

# Buy wager endpoint
- Enjoy the `BuyWagerLock` and `BuyWagerLockTest`class
  - The current setting is using `database` to solve the concurrency problem
