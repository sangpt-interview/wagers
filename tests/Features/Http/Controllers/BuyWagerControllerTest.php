<?php

namespace Tests\Features\Http\Controllers;

use App\Exceptions\BuyWagerError;
use App\Models\Buy;
use App\Models\Wager;
use App\Services\BuyWagerService;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class BuyWagerControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        Wager::create([
            'total_wager_value' => 100,
            'odds' => 2,
            'selling_percentage' => 60,
            'selling_price' => 65,
            'current_selling_price' => 65,
        ]);
    }

    public function testBuyWagerSuccess()
    {
        /** @var TestResponse $response */
        $http = $this->json('POST', 'buy/1', [
            'buying_price' => 10,
        ]);

        $http->response->assertStatus(201);
        $http->response->assertJsonStructure([
            'id',
            'wager_id',
            'buying_price',
            'bought_at',
        ]);
        $http->response->assertJsonFragment([
            'id' => 1,
            'wager_id' => 1,
            'buying_price' => 10,
        ]);

        $this->assertSame(1, Buy::query()->count());
    }

    public function testWagerIdNotPresentInUri()
    {
        /** @var TestResponse $response */
        $http = $this->json('POST', '/buy', ['buying_price' => 10]);
        $http->response->assertStatus(500);
        $http->response->assertJsonStructure([
            'error'
        ]);
        $http->response->assertSeeText([
            'Error.'
        ]);
    }

    /**
     * @dataProvider invalidRequestsDataProvider
     */
    public function testValidation($wagerId, $buyingPrice)
    {
        /** @var TestResponse $response */
        $http = $this->json('POST', '/buy/' . $wagerId, ['buying_price' => $buyingPrice]);
        $http->response->assertStatus(422);
        $http->response->assertJsonStructure([
            'error'
        ]);
    }

    public function invalidRequestsDataProvider(): array
    {
        return [
            // Wager not existing
            ['fasdf', 10],
            // Wager not existing
            [2, 10],
            // buying_price not present
            [1, null],
            // buying_price not positive decimal
            [1, -10],
            // buying_price not positive decimal
            [1, 0],
            // buying_price not positive decimal
            [1, 'fasdfsd'],
        ];
    }

    public function testBuyingPriceInvalid()
    {
        /** @var TestResponse $response */
        $http = $this->json('POST', '/buy/1', ['buying_price' => 100]);

        $http->response->assertStatus(422);
        $http->response->assertJsonStructure([
            'error'
        ]);
        $http->response->assertSeeText("Buying price must be lesser or equal to 65");
    }

    public function testGotUnexpectedError()
    {
        $mock = $this->createMock(BuyWagerService::class);
        $mock->method('execute')->willThrowException(new BuyWagerError('Database Error'));
        $this->app->offsetSet(BuyWagerService::class, $mock);

        /** @var TestResponse $response */
        $http = $this->json('POST', '/buy/1', ['buying_price' => 10]);

        $http->response->assertStatus(500);
        $http->response->assertJsonStructure([
            'error'
        ]);
        $http->response->assertSeeText("Error.");
    }
}
