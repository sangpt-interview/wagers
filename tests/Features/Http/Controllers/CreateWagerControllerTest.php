<?php

namespace Tests\Features\Http\Controllers;

use App\Exceptions\CreateWagerError;
use App\Models\Wager;
use App\Services\CreateWagerService;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class CreateWagerControllerTest extends TestCase
{
    public function testCreateWagerSuccess()
    {
        /** @var TestResponse $response */
        $http = $this->json('POST', '/wagers', [
            'total_wager_value' => 100,
            'odds' => 2,
            'selling_percentage' => 60,
            'selling_price' => 65,
        ]);

        $http->response->assertStatus(201);
        $http->response->assertJsonStructure([
            'id',
            'total_wager_value',
            'odds',
            'selling_percentage',
            'selling_price',
            'current_selling_price',
            'percentage_sold',
            'amount_sold',
            'placed_at'
        ]);
        $http->response->assertJsonFragment([
            'id' => 1,
            'total_wager_value' => 100,
            'odds' => 2,
            'selling_percentage' => 60,
            'selling_price' => 65.00,
            'current_selling_price' => 65.00,
            'percentage_sold' => null,
            'amount_sold' => null,
        ]);

        $this->assertSame(1, Wager::query()->count());
    }

    /**
     * @dataProvider invalidRequestsDataProvider
     */
    public function testValidation($data)
    {
        /** @var TestResponse $response */
        $http = $this->json('POST', '/wagers', $data);
        $http->response->assertStatus(422);
        $http->response->assertJsonStructure([
            'error'
        ]);
    }

    public function invalidRequestsDataProvider(): array
    {
        return [
            [
                // total_wager_value empty
                [
                    'odds' => 2,
                    'selling_percentage' => 60,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // total_wager_value not integer
                [
                    'total_wager_value' => 'fasdfas',
                    'odds' => 2,
                    'selling_percentage' => 60,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // total_wager_value not integer
                [
                    'total_wager_value' => 100.90,
                    'odds' => 2,
                    'selling_percentage' => 60,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // total_wager_value not gt 0
                [
                    'total_wager_value' => 0,
                    'odds' => 2,
                    'selling_percentage' => 60,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // odds empty
                [
                    'total_wager_value' => 100,
                    'selling_percentage' => 60,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // odds not integer
                [
                    'total_wager_value' => 100,
                    'odds' => 'fasd',
                    'selling_percentage' => 60,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // odds not integer
                [
                    'total_wager_value' => 100,
                    'odds' => 2.09,
                    'selling_percentage' => 60,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // odds not gt 0
                [
                    'total_wager_value' => 100,
                    'odds' => 0,
                    'selling_percentage' => 60,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // selling_percentage empty
                [
                    'total_wager_value' => 100,
                    'odds' => 2,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // selling_percentage not integer
                [
                    'total_wager_value' => 100,
                    'odds' => 2,
                    'selling_percentage' => 'dsfasd',
                    'selling_price' => 65.00,
                ],
            ],
            [
                // selling_percentage not integer
                [
                    'total_wager_value' => 100,
                    'odds' => 2,
                    'selling_percentage' => 10.09,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // selling_percentage not gt 0
                [
                    'total_wager_value' => 100,
                    'odds' => 2,
                    'selling_percentage' => 0,
                    'selling_price' => 65.00,
                ],
            ],
            [
                // selling_price empty
                [
                    'total_wager_value' => 100,
                    'odds' => 2,
                    'selling_percentage' => 60,
                ],
            ],
            [
                // selling_price not numeric
                [
                    'total_wager_value' => 100,
                    'odds' => 2,
                    'selling_percentage' => 60,
                    'selling_price' => 'dfasdf',
                ],
            ],
            [
                // selling_price not gt 0
                [
                    'total_wager_value' => 100,
                    'odds' => 2,
                    'selling_percentage' => 60,
                    'selling_price' => 0,
                ],
            ],
        ];
    }

    public function testSellingPriceInvalid()
    {
        /** @var TestResponse $response */
        $http = $this->json('POST', '/wagers', [
            'total_wager_value' => 100,
            'odds' => 2,
            'selling_percentage' => 60,
            'selling_price' => 60,
        ]);

        $http->response->assertStatus(422);
        $http->response->assertJsonStructure([
            'error'
        ]);
        $http->response->assertSeeText("Selling price must be greater than 60");
    }

    public function testGotUnexpectedError()
    {
        $mock = $this->createMock(CreateWagerService::class);
        $mock->method('execute')->willThrowException(new CreateWagerError('Database Error'));
        $this->app->offsetSet(CreateWagerService::class, $mock);

        /** @var TestResponse $response */
        $http = $this->json('POST', '/wagers', [
            'total_wager_value' => 100,
            'odds' => 2,
            'selling_percentage' => 60,
            'selling_price' => 61,
        ]);

        $http->response->assertStatus(500);
        $http->response->assertJsonStructure([
            'error'
        ]);
        $http->response->assertSeeText("Error.");
    }
}
