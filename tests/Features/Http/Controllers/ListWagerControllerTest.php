<?php

namespace Tests\Features\Http\Controllers;

use App\Models\Wager;
use Tests\TestCase;

class ListWagerControllerTest extends TestCase
{
    public function testListWagerSuccess()
    {
        $this->prepareWager();
        $this->prepareWager();

        $http = $this->json('GET', '/wagers');
        $http->response->assertStatus(200);
        $http->response->assertJsonStructure([
            '*' => [
                'id',
                'total_wager_value',
                'odds',
                'selling_percentage',
                'selling_price',
                'current_selling_price',
                'percentage_sold',
                'amount_sold',
                'placed_at'
            ]
        ]);
        $http->response->assertJsonCount(2);

        $http = $this->json('GET', '/wagers?limit=1');
        $http->response
            ->assertStatus(200)
            ->assertJsonCount(1);

        $http = $this->json('GET', '/wagers?page=2&limit=3');
        $http->response
            ->assertStatus(200)
            ->assertJsonCount(0);
    }

    protected function prepareWager(): void
    {
        Wager::create([
            'total_wager_value' => 100,
            'odds' => 2,
            'selling_percentage' => 60,
            'selling_price' => 65,
            'current_selling_price' => 65,
        ]);
    }
}
