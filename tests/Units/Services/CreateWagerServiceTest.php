<?php

namespace Tests\Units\Services;

use App\Exceptions\CreateWagerError;
use App\Exceptions\SellingPriceInvalid;
use App\Services\CreateWagerService;
use App\Services\Requests\CreateWagerRequest;
use Tests\TestCase;

class CreateWagerServiceTest extends TestCase
{
    private CreateWagerService $createWagerService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->createWagerService = app(CreateWagerService::class);
    }

    public function testCreateWagerSuccess()
    {
        $request = $this->fakeCreateWagerRequest();
        $wager = $this->createWagerService->execute($request);

        $this->assertSame(1, $wager->id);
        $this->assertSame(100, $wager->total_wager_value);
        $this->assertSame(2, $wager->odds);
        $this->assertSame(60, $wager->selling_percentage);
        $this->assertSame(65.00, $wager->selling_price);
        $this->assertSame(65.00, $wager->current_selling_price);
        $this->assertNull($wager->percentage_sold);
        $this->assertNull($wager->amount_sold);
    }

    /**
     * @dataProvider listCreatWagerRequestToError
     */
    public function testCreateWagerError(CreateWagerRequest $request)
    {
        $this->expectException(CreateWagerError::class);
        $this->createWagerService->execute($request);
    }

    public function listCreatWagerRequestToError(): array
    {
        return [
            // total_wager_value column must be unsigned integer
            [
                $this->fakeCreateWagerRequest(totalWagerValue: -1)
            ],
            // odds must be unsigned integer
            [
                $this->fakeCreateWagerRequest(odds: -1)
            ],
            // selling_percentage must be unsigned integer
            [
                $this->fakeCreateWagerRequest(sellingPercentage: -1)
            ],
        ];
    }

    public function testSellingPriceInvalid()
    {
        $this->expectException(SellingPriceInvalid::class);
        $this->createWagerService->execute($this->fakeCreateWagerRequest(sellingPrice: 50));
    }

    protected function fakeCreateWagerRequest($totalWagerValue = 100, $odds = 2, $sellingPercentage = 60, $sellingPrice = 65): CreateWagerRequest
    {
        return new CreateWagerRequest($totalWagerValue, $odds, $sellingPercentage, $sellingPrice);
    }
}
