<?php

namespace Tests\Units\Services;

use App\Models\Wager;
use App\Services\ListWagerService;
use App\Services\Requests\ListWagerRequest;
use Tests\TestCase;

class ListWagerServiceTest extends TestCase
{
    private ListWagerService $listWagerService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->listWagerService = app(ListWagerService::class);

        for ($i = 0; $i < 10; $i++) {
            $this->prepareWager();
        }
    }

    /**
     * @dataProvider listWagerRequests
     */
    public function testGetListWager(ListWagerRequest $request, $expectedCount, $expectedFirstId, $expectLastId)
    {
        $wagers = $this->listWagerService->execute($request);
        $this->assertSame($expectedCount, $wagers->count());
        $this->assertSame($expectedFirstId, $wagers->first()?->id);
        $this->assertSame($expectLastId, $wagers->last()?->id);
    }

    public function listWagerRequests(): array
    {
        return [
            [$this->fakeListWagerRequest(0, 2), 2, 1, 2],
            [$this->fakeListWagerRequest(1, 3), 3, 4, 6],
            [$this->fakeListWagerRequest(2, 4), 2, 9, 10],
            [$this->fakeListWagerRequest(3, 4), 0, null, null],
        ];
    }

    protected function prepareWager(): void
    {
        Wager::create([
            'total_wager_value' => 100,
            'odds' => 2,
            'selling_percentage' => 60,
            'selling_price' => 65,
            'current_selling_price' => 65,
        ]);
    }

    protected function fakeListWagerRequest($page = 0, $limit = 1): ListWagerRequest
    {
        return new ListWagerRequest($page, $limit);
    }
}
