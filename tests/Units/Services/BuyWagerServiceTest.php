<?php

namespace Tests\Units\Services;

use App\Exceptions\BuyingPriceInvalid;
use App\Exceptions\BuyWagerError;
use App\Models\Wager;
use App\Services\BuyWagerService;
use App\Services\Requests\BuyWagerRequest;
use Tests\TestCase;

class BuyWagerServiceTest extends TestCase
{
    private BuyWagerService $buyWagerService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->buyWagerService = app(BuyWagerService::class);
        $this->prepareWager();
    }

    public function testBuyingPriceInvalid()
    {
        $this->expectException(BuyingPriceInvalid::class);
        $this->buyWagerService->execute($this->fakeBuyWagerRequest(1, 66));
    }

    /**
     * @dataProvider listBuyWagerRequestToError
     */
    public function testBuyWagerError(BuyWagerRequest $request)
    {
        $this->expectException(BuyWagerError::class);
        $this->buyWagerService->execute($request);
    }

    public function listBuyWagerRequestToError(): array
    {
        return [
            // Wager not existing
            [
                $this->fakeBuyWagerRequest(2, 10)
            ],
            // buying_price must be unsigned decimal
            [
                $this->fakeBuyWagerRequest(1, -10)
            ],
        ];
    }

    public function testBuyWagerSuccess()
    {
        $buy = $this->buyWagerService->execute($this->fakeBuyWagerRequest(1, 10.09));
        $this->assertSame(1, $buy->id);
        $this->assertSame(1, $buy->wager_id);
        $this->assertSame(10.09, $buy->buying_price);

        $wager = Wager::find(1);

        $this->assertSame(65 - 10.09, $wager->current_selling_price);
        $this->assertSame(10.09, $wager->amount_sold);
        $this->assertSame(round((10.09 / 65) * 100, 2), $wager->percentage_sold);
    }

    protected function prepareWager(): void
    {
        Wager::create([
            'total_wager_value' => 100,
            'odds' => 2,
            'selling_percentage' => 60,
            'selling_price' => 65,
            'current_selling_price' => 65,
        ]);
    }

    protected function fakeBuyWagerRequest($wagerId, $buyingPrice): BuyWagerRequest
    {
        return new BuyWagerRequest($wagerId, $buyingPrice);
    }
}
