<?php

namespace Tests\Units\Services\Locks;

use App\Services\Locks\BuyWagerLock;
use Illuminate\Contracts\Cache\LockTimeoutException;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class BuyWagerLockTest extends TestCase
{
    private BuyWagerLock $lock;
    private string $lockKey = 'testBUY_WAGER_1_LOCK';

    protected function setUp(): void
    {
        parent::setUp();

        config([
            'lock.buy_wager.expired_id' => 1,
            'lock.buy_wager.block_id' => 0.5,
        ]);

        $this->lock = new BuyWagerLock(1);
    }

    public function testCanLock()
    {
        $result = $this->lock->block();

        $this->assertNotEmpty($result);
        $this->assertSame(1, DB::table('cache_locks')->where('key', $this->lockKey)->count());
    }

    public function testLockGotTimeout()
    {
        $this->lock->block();

        sleep(0.5);

        $this->expectException(LockTimeoutException::class);
        (new BuyWagerLock(1))->block();
    }

    public function testRelease()
    {
        $this->lock->block();
        $this->lock->release();

        $this->assertSame(0, DB::table('cache_locks')->where('key', $this->lockKey)->count());
    }

    public function testLockReleasedThenCanLock()
    {
        $this->lock->block();
        sleep(0.5);
        $this->lock->release();

        $result = (new BuyWagerLock(1))->block();
        $this->assertNotEmpty($result);
        $this->assertSame(1, DB::table('cache_locks')->where('key', $this->lockKey)->count());
    }

    public function testLockReleasedItself()
    {
        $this->lock->block();
        sleep(1.2);

        $result = (new BuyWagerLock(1))->block();
        $this->assertNotEmpty($result);
        $this->assertSame(1, DB::table('cache_locks')->where('key', $this->lockKey)->count());
    }
}
