<?php

namespace Tests;

use Laravel\Lumen\Application;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use DatabaseMigrations;

    /**
     * Creates the application.
     *
     * @return Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    public function runDatabaseMigrations()
    {
        $this->artisan('migrate:fresh');

        $this->beforeApplicationDestroyed(function () {
            $this->artisan('migrate:fresh');
        });
    }
}
